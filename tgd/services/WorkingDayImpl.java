package com.lgim.qa.services;

import java.io.FileInputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * Implementation to retrieve Bank Holidays from local JSON file.
 */
@Lazy
@Service("workingDayJson")
public class WorkingDayImpl implements WorkingDay {

    private static final String DATE = "date";
    private static final Logger logger = LoggerFactory.getLogger(WorkingDayImpl.class);
    private static final String ENGLAND_AND_WALES = "england-and-wales";
    private static final String DIVISION = "division";
    private static final String EVENTS = "events";
    private static final String BANK_HOLIDAYS_FILENAME = "src//test//resources//bankholidays.json";

    /**
     * {@inheritDoc}
     */
    public LocalDate getNextWorkingDayAfterDate(LocalDate start) {
        List<LocalDate> bankHolidays = getEnglandAndWalesBankHolidays();
        logger.debug("Date we are searching forwards from {}", start);
        LocalDate next = start;
        while ((next = next.plusDays(1)).isBefore(start.plusDays(7))) {
            if (isWorkingDay(next, bankHolidays)) {
                logger.debug("Nearest Subsequent working day is {}", next);
                return next;
            }
        }
        return null;
    }

    /*
     Method to calculate the working days.
     */
    private boolean isWorkingDay(LocalDate date, List<LocalDate> bankHolidays) {
        // Firstly check if we have a week day..
        if (date.getDayOfWeek().getValue() > 5) {
            logger.debug("Date is on the weekend so not a working day {}", date);
            return false;
        }

        // Then check if the date is a bank holiday..
        if (!CollectionUtils.isEmpty(bankHolidays)) {
            List<LocalDate> matchedBankHolidays = bankHolidays.stream()
                    .filter(bankHoliday -> bankHoliday.isEqual(date)).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(matchedBankHolidays)) {
                logger.debug("We have a bank holiday so not a working day {}", matchedBankHolidays.get(0));
                return false;
            } else {
                logger.debug("Date is not a bank holiday or weekend so is a working day!");
                return true;
            }
        } else {
            throw new RuntimeException("Bank Holiday List is empty");
        }
    }

    /**
     * {@inheritDoc}
     */
    public LocalDate getNearestWorkingDayBeforeDate(LocalDate start) {
        List<LocalDate> bankHolidays = getEnglandAndWalesBankHolidays();
        logger.debug("Date we are searching backwards from {}", start);
        LocalDate next = start;
        while ((next = next.minusDays(1)).isAfter(start.minusDays(7))) {
            if (isWorkingDay(next, bankHolidays)) {
                logger.debug("Nearest Previous working day is {}", next);
                return next;
            }
        }

        return null;
    }

    /**
     * Method to extract the Bank Holidays from the local JSON file containing Bank Holidays.
     *
     * @return the list of Bank Holidays for England and Wales
     */
    protected List<LocalDate> getEnglandAndWalesBankHolidays() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        List<LocalDate> bankHolidays = new ArrayList<>();

        JsonNode root;
        try {
            root = mapper.readTree(new FileInputStream(BANK_HOLIDAYS_FILENAME));

            Iterator<JsonNode> items = root.getElements();
            while (items.hasNext()) {
                JsonNode parameterNode = items.next();
                if (StringUtils.equals(parameterNode.get(DIVISION).getTextValue(), ENGLAND_AND_WALES)) {
                    logger.debug("Parsing England & Wales nodes..");
                    Iterator<JsonNode> eventNodes = parameterNode.path(EVENTS).getElements();
                    while (eventNodes.hasNext()) {
                        JsonNode eventNode = eventNodes.next();
                        bankHolidays.add(LocalDate.parse(eventNode.get(DATE).getTextValue()));
                        logger.debug("Added event with date {} to list of Bank Holidays",
                                eventNode.get(DATE).getTextValue());
                    }

                    break; // we don't want to parse other regions
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Error parsing the bank holidays json file.");
        }
        return bankHolidays;
    }
}