package com.lgim.qa.services;

import java.time.LocalDate;

public interface WorkingDay {

    /**
     * Get the nearest working day before the specified date.
     * @param date - the date to start searching backwards from
     * @return the nearest working date
     */
    LocalDate getNearestWorkingDayBeforeDate(LocalDate date);

    /**
     * Get the nearest working day after the specified date.
     * @param date - the date to start searching forwards from
     * @return the next working date
     */
    LocalDate getNextWorkingDayAfterDate(LocalDate date);
}
