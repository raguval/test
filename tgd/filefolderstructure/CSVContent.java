package com.lgim.qa.filefolderstructure;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This class will collect the key, values of test data and save in csv file.
 */

@Lazy
@Component
@Scope("cucumber-glue")
public class CSVContent {
    public Map<String, String> getCsvValues() {
        return csvValues;
    }

    public void setCsvValues(Map<String, String> csvValues) {
        this.csvValues = csvValues;
    }

    private Map<String, String> csvValues = new HashMap<String, String>();
}