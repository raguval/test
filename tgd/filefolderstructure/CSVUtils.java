package com.lgim.qa.filefolderstructure;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This class creates the CSV file and folder structure for test data.
 */
@Lazy
@Component
@Scope("cucumber-glue")
public class CSVUtils {

    @Autowired
    private CSVContent csvContent;

    @Value("${test.data.path}")
    private String testDataPath;

    private static final Logger logger = LoggerFactory.getLogger(CSVUtils.class);

    public void readCSVFile(String folderName, String fileName) throws IOException {

        Reader reader = Files.newBufferedReader(Paths.get(testDataPath + folderName + "\\" + fileName + ".csv"));

        CSVReader csvReader = new CSVReader(reader);

        List<String[]> listOfCSVContent = csvReader.readAll();

        String[] header = listOfCSVContent.get(0);
        String[] values = listOfCSVContent.get(1);

        for (int i = 0; i < header.length; i++) {
            csvContent.getCsvValues().put(header[i], values[i]);
        }
    }

    public void createCSVFile(String folderName) throws IOException {

        File folder = new File(testDataPath + folderName);

        if (writeDir(folder)) {

            try (
                    Writer writer = Files.newBufferedWriter(Paths.get(folder + "\\Leaver Scheme "
                            + csvContent.getCsvValues().get("scheme Id") + ".csv"));
                    CSVWriter csvWriter = new CSVWriter(writer,
                            CSVWriter.DEFAULT_SEPARATOR,
                            CSVWriter.NO_QUOTE_CHARACTER,
                            CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                            CSVWriter.DEFAULT_LINE_END)
            ) {
                String[] headerRecord = csvContent.getCsvValues().keySet().toArray(new String[0]);
                csvWriter.writeNext(headerRecord);
                csvWriter.writeNext(csvContent.getCsvValues().values().toArray(new String[0]));
                logger.info("Test data created successfully");
            }
        }
    }

    private Boolean writeDir(File folder) {

        if (!folder.exists()) {
            return folder.mkdir();
        }
        return false;
    }
}