package com.lgim.qa.xray;

import java.io.File;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Optional;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Utils {
    private static final Logger logger = LoggerFactory.getLogger(Utils.class);
    private static final String X_ACCESS_TOKEN = "x-access-token";

    static Optional<String> getEnvProp(String name) {
        String highPriority = System.getProperty(name);
        String lowPriority = System.getenv(name);

        if (highPriority != null) {
            return Optional.of(highPriority);
        }

        if (lowPriority != null) {
            return Optional.of(lowPriority);
        }

        return Optional.empty();
    }

    /*
       * Get a JWT Token from the XRay API
     */
    static String getJWTToken(Client client, String xrayClientId, String xrayClientSecret) {
        WebTarget target = client.target(XRayInstance.getXrayRootUrl() + "/authenticate");
        String body = "{\"client_id\": \"" + xrayClientId + "\", \"client_secret\":\""
                + xrayClientSecret + "\"}";

        Response response = target.request("application/json").post(Entity.json(body));
        logger.info(XRayInstance.getXrayRootUrl());
        if (response.getStatus() == HttpStatus.SC_OK) {
            // We should now have the JWT Token in a Header..
            return response.getHeaderString(X_ACCESS_TOKEN);
        } else {
            throw new RuntimeException("Unable to get a new JWT Token to access REST API " + response.getStatus());
        }
    }

    private static String base64Encoded(String username, String password) {
        String authString = username + ":" + password;
        byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
        return new String(authEncBytes);
    }

    static String basicEncoded(String username, String password) {
        return "Basic " + base64Encoded(username, password);
    }

    static String readFile(String filename) throws IOException {
        File file = new File(filename);
        return new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), StandardCharsets.UTF_8);
    }

    /*
     * Get an SSL Client.
     */
    static Client getSSLClient() throws KeyManagementException, NoSuchAlgorithmException {
        logger.info("Getting the SSL client");
        SSLContext context;

        final TrustManager[] trustManagerArray = {new NullX509TrustManager()};
        context = SSLContext.getInstance("TLSv1.2");
        context.init(null, trustManagerArray, null);

        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(MultiPartFeature.class);
        return ClientBuilder.newBuilder()
                .withConfig(clientConfig)
                .sslContext(context)
                .hostnameVerifier(new NullHostnameVerifier())
                .register(MultiPartFeature.class)
                .build();
    }

    /*
    * Trust manager that does not perform any SSL checks.
    */
    private static class NullX509TrustManager implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    /*
     * Host name verifier that does not perform nay checks.
     */
    private static class NullHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    static void useProxy() {
        System.setProperty("https.proxyHost", XRayInstance.getProxyHost());
        System.setProperty("https.proxyPort", XRayInstance.getProxyPort());

        System.setProperty("http.proxyHost", XRayInstance.getProxyHost());
        System.setProperty("http.proxyPort", XRayInstance.getProxyPort());

        Authenticator.setDefault(
                new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                XRayInstance.getProxyUsername(), XRayInstance.getProxyPassword().toCharArray()
                        );
                    }
                }
        );
    }
}