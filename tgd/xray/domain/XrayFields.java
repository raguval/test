package com.lgim.qa.xray.domain;

/**
 * Created by JacksonGenerator on 05/06/19.
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class XrayFields {

    @JsonProperty("testPlanKey")
    private String testPlanKey;

    public void setTestPlanKey(String testPlanKey) {
        this.testPlanKey = testPlanKey;
    }

    public String getTestPlanKey() {
        return testPlanKey;
    }
}