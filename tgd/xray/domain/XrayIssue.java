package com.lgim.qa.xray.domain;

/**
 * Created by JacksonGenerator on 05/06/19.
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class XrayIssue {

    @JsonProperty("fields")
    private Fields fields;

    @JsonProperty("xrayFields")
    private XrayFields xrayFields;

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public void setXrayFields(XrayFields xrayFields) {
        this.xrayFields = xrayFields;
    }

    public Fields getFields() {
        return fields;
    }

    public XrayFields getXrayFields() {
        return xrayFields;
    }
}