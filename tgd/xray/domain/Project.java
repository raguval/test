package com.lgim.qa.xray.domain;

/**
 * Created by JacksonGenerator on 05/06/19.
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class Project {

    @JsonProperty("key")
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}