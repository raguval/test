package com.lgim.qa.xray.domain;

/**
 * Created by JacksonGenerator on 05/06/19.
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class Fields {

    @JsonProperty("summary")
    private String summary;

    @JsonProperty("issuetype")
    private Issuetype issuetype;

    @JsonProperty("project")
    private Project project;

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setIssuetype(Issuetype issuetype) {
        this.issuetype = issuetype;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getSummary() {
        return summary;
    }

    public Issuetype getIssuetype() {
        return issuetype;
    }

    public Project getProject() {
        return project;
    }
}