package com.lgim.qa.xray.domain;

/**
 * Created by JacksonGenerator on 05/06/19.
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class Issuetype {

    @JsonProperty("id")
    private String id;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}