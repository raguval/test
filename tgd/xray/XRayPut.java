package com.lgim.qa.xray;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class gets called as part of the Maven post-integration-test phase to
 * call the XRay endpoint to import the cucumber execution report back into XRay.
 */
public class XRayPut {
    private static final Logger logger = LoggerFactory.getLogger(XRayPut.class);
    private static final String XRAY_REST_IMPORT_URL = XRayInstance.getXrayRootUrl() + "/import/execution/cucumber";
    private static final String XRAY_REST_IMPORT_MULTI_URL = XRayInstance.getXrayRootUrl()
            + "/import/execution/cucumber/multipart";
    private static final String CUCUMBER_REPORT_FILE_LOCATION = "target/cucumber-report.json";

    /**
     * Method get's invoked to write the test results back to XRay.
     * @param args - Runtime params
     * @throws IOException Thrown if there is a file operation failure
     * @throws NoSuchAlgorithmException Thrown when a particular cryptographic algorithm is not available in the environment.
     * @throws KeyManagementException thrown if there are any key management issue getting SSL context.
     */
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyManagementException {

        // Ensure that the Cucumber Report file has been generated before progressing
        File resultFile = new File(CUCUMBER_REPORT_FILE_LOCATION);
        if (resultFile.isFile()) {
            Utils.useProxy();

            // Get a JWT Token to use for the API calls..
            final Client client = Utils.getSSLClient();
            String xrayClientId = XRayInstance.getXrayClientId();
            String xrayClientSecret = XRayInstance.getXrayClientSecret();

            String token = Utils.getJWTToken(client, xrayClientId, xrayClientSecret);
            logger.info("JWT Token is {}", token);

            // Check the existence of the info file, if it's there, we need to create a Multipart request to
            // register the test run against a Test Plan ..
            Entity entity;
            WebTarget target;
            File infoFile = new File(XRayInstance.INFO_URL);

            if (infoFile.isFile()) {
                logger.info("URL is {}", XRAY_REST_IMPORT_MULTI_URL);
                entity = makeEntityFromFiles(resultFile, infoFile);
                target = client.target(XRAY_REST_IMPORT_MULTI_URL);
            } else {
                logger.info("URL is {}", XRAY_REST_IMPORT_URL);
                entity = makeEntityFromResultsFile();
                target = client.target(XRAY_REST_IMPORT_URL);
            }

            // Call the XRay endpoint to update the test results in XRay
            Response response;
            response = target.request()
                    .header("Authorization", "Bearer " + token)
                    .accept("*/*")
                    .post(entity);

            if (response.getStatus() == HttpStatus.SC_OK) {
                String body = response.readEntity(String.class);
                logger.info(body);
            } else {
                throw new RuntimeException("An error has occurred updating XRay, response code " + response.getStatus());
            }
        } else {
            throw new RuntimeException("No Cucumber report available to process.");
        }
    }

    /*
     * Use the Cucumber report file to build the XRay request.
     * @return the JSON entity
     * @throws IOException thrown if there is a conversion error from Cucumber file to JSON entity.
     */
    private static Entity makeEntityFromResultsFile() throws IOException {
        String cucumberReport = Utils.readFile(CUCUMBER_REPORT_FILE_LOCATION);
        return Entity.json(cucumberReport);
    }

    /*
     * Use the Cucumber report and Xray info file to build a Multipart request.
     *
     * @param cucumberFile the cucumber file to add to the request.
     * @param infoFile the info file to add to the request.
     * @return the multipart entity to send in the request.
     */
    private static Entity makeEntityFromFiles(File cucumberFile, File infoFile) {
        logger.info("Multipart XRay request");
        FormDataMultiPart multiPart = new FormDataMultiPart();

        // add the result file
        FileDataBodyPart resultPart = new FileDataBodyPart("results", cucumberFile, MediaType.APPLICATION_OCTET_STREAM_TYPE);
        multiPart.getBodyParts().add(resultPart);

        // add the control file
        FileDataBodyPart infoPart = new FileDataBodyPart("info", infoFile, MediaType.APPLICATION_OCTET_STREAM_TYPE);
        multiPart.getBodyParts().add(infoPart);
        return Entity.entity(multiPart, multiPart.getMediaType());
    }
}