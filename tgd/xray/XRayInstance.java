package com.lgim.qa.xray;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class XRayInstance {
    private static final Logger logger = LoggerFactory.getLogger(XRayInstance.class);

    static {
        // Ensure that all the command line arguments have been received before continuing..
        logger.info("Checking input parameters..");
        getXrayTests();
        getXrayRootUrl();
        getXrayRootUrl();
        getXrayExecutionTypeId();
        getXrayClientId();
        getXrayClientSecret();
        getJiraUserId();
        getJiraUserAPIKey();
        getProxyHost();
        getProxyPort();
        getProxyUsername();
        getProxyPassword();
        logger.info("All expected input parameters provided..");
    }

    static final String INFO_URL = "target/xray-info.json";

    static String getXrayTests()  {
        Optional<String> jiraXrayTests = Utils.getEnvProp("XRAY_TESTS");
        if (!(jiraXrayTests.isPresent())) {
            throw new RuntimeException("JIRA XRay Tests to execute have not been provided");
        }

        // Determine if we have more than 1 Issue Type - if we do - reject..
        List<String> issueTypes = Arrays.asList(jiraXrayTests.get().split(";"));

        if (issueTypes.size() > 1) {
            throw new RuntimeException("More that one XRay issue type has been provided, the platform only supports one at a "
                    + "time so please revise the input parameter");
        }

        return jiraXrayTests.get();
    }

    static String getXrayRootUrl()  {
        Optional<String> jiraXrayRootUrl = Utils.getEnvProp("XRAY_ROOT_URL");
        if (!(jiraXrayRootUrl.isPresent())) {
            throw new RuntimeException("JIRA XRay URL has not been provided");
        }
        return jiraXrayRootUrl.get();
    }

    static String getXrayExecutionTypeId()  {
        Optional<String> jiraXrayExecutionTypeId = Utils.getEnvProp("XRAY_EXECUTION_TYPE_ID");
        if (!(jiraXrayExecutionTypeId.isPresent())) {
            throw new RuntimeException("JIRA Execution Type ID has not been provided");
        }
        return jiraXrayExecutionTypeId.get();
    }

    static String getXrayClientId()  {
        Optional<String> xrayClientId = Utils.getEnvProp("XRAY_CLIENT_ID");
        if (!(xrayClientId.isPresent())) {
            throw new RuntimeException("JIRA XRay Client ID has not been provided");
        }
        return xrayClientId.get();
    }

    static String getXrayClientSecret()  {
        Optional<String> xrayClientSecret = Utils.getEnvProp("XRAY_CLIENT_SECRET");
        if (!(xrayClientSecret.isPresent())) {
            throw new RuntimeException("JIRA XRay Client Secret has not been provided");
        }
        return xrayClientSecret.get();
    }

    static String getJiraUserId()  {
        Optional<String> jiraUserId = Utils.getEnvProp("JIRA_USER_ID");
        if (!(jiraUserId.isPresent())) {
            throw new RuntimeException("JIRA User ID has not been provided");
        }
        return jiraUserId.get();
    }

    static String getJiraUserAPIKey()  {
        Optional<String> jiraUserApiKey = Utils.getEnvProp("JIRA_USER_API_KEY");
        if (!(jiraUserApiKey.isPresent())) {
            throw new RuntimeException("JIRA API Key has not been provided");
        }
        return jiraUserApiKey.get();
    }

    static String getProxyHost()  {
        Optional<String> proxyHost = Utils.getEnvProp("PROXY_HOST");
        if (!(proxyHost.isPresent())) {
            throw new RuntimeException("Proxy Host has not been provided");
        }
        return proxyHost.get();
    }

    static String getProxyPort()  {
        Optional<String> proxyPort = Utils.getEnvProp("PROXY_PORT");
        if (!(proxyPort.isPresent())) {
            throw new RuntimeException("Proxy Port has not been provided");
        }
        return proxyPort.get();
    }

    static String getProxyUsername()  {
        Optional<String> proxyUsername = Utils.getEnvProp("PROXY_USERNAME");
        if (!(proxyUsername.isPresent())) {
            throw new RuntimeException("Proxy Username has not been provided");
        }
        return proxyUsername.get();
    }

    static String getProxyPassword()  {
        Optional<String> proxyPassword = Utils.getEnvProp("PROXY_PASSWORD");
        if (!(proxyPassword.isPresent())) {
            throw new RuntimeException("Proxy Password has not been provided");
        }
        return proxyPassword.get();
    }

    static String getJiraRootUrl()  {
        Optional<String> jiraXrayRootUrl = Utils.getEnvProp("JIRA_ROOT_URL");
        if (!(jiraXrayRootUrl.isPresent())) {
            throw new RuntimeException("JIRA Root URL has not been provided");
        }
        return jiraXrayRootUrl.get();
    }
}