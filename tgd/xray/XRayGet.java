package com.lgim.qa.xray;

import static java.lang.System.exit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lgim.qa.xray.domain.Fields;
import com.lgim.qa.xray.domain.Issuetype;
import com.lgim.qa.xray.domain.Project;
import com.lgim.qa.xray.domain.XrayFields;
import com.lgim.qa.xray.domain.XrayIssue;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class gets called as part of the Maven pre-integration-test phase to
 * call the XRay endpoint to export the relevant feature files from.  The response
 * from XRay is in a zip format, so the file is unzipped and the tests executed
 * from the target folder which will be refreshed on every build.
 */
public class XRayGet {
    private static final Logger logger = LoggerFactory.getLogger(XRayGet.class);
    private static final String XRAY_REST_EXPORT_URL = XRayInstance.getXrayRootUrl() + "/export/cucumber";
    private static final String XRAY_FEATURES_ZIP_PATH = "./target/test/features/xray.features.zip";
    private static final String XRAY_FEATURE_FOLDER = "./target/test/features/";
    private static final String TEST_FOLDER_LOCATION = "target/test";
    private static final String TEST_PLAN = "Test Plan";

    public static void main(String[] args) throws IOException, KeyManagementException, NoSuchAlgorithmException {

        // Access via proxy server
        Utils.useProxy();

        // Create an auth token for the JIRA user to connect to JIRA
        String jiraUserId = XRayInstance.getJiraUserId();
        String jiraUserAPIKey = XRayInstance.getJiraUserAPIKey();
        String basicAuth = Utils.basicEncoded(jiraUserId, jiraUserAPIKey);
        logger.info("Basic auth Token is {}", basicAuth);

        // Determine whether we need to run a filter query or just use the JIRA issue
        String xrayTests = XRayInstance.getXrayTests();
        final Client client = Utils.getSSLClient();
        if (XRayInstance.getXrayTests().startsWith("filter")) {
            // We know we have a filter passed to us to locate the first item from the filter..
            xrayTests = getFirstIssueFromFilter(XRayInstance.getXrayTests(), basicAuth, client);

            // After retrieving the issue to process from the filter, update it to In Progress
            // it isn't picked up by the filter query again next time it's run.
            updateJiraIssueStatus(xrayTests, basicAuth, client);
        }

        // Build folder structure
        makeTargetFeaturesDirectory();

        // If the issue type is a Test Plan, we need to create an info file and append the Test Plan
        // Id in order to associate them correctly in XRay.
        createInfoFileIfRequired(xrayTests, client, basicAuth);

        // Get a JWT Token to use for the XRay API calls..
        String xrayClientId = XRayInstance.getXrayClientId();
        String xrayClientSecret = XRayInstance.getXrayClientSecret();
        String token = Utils.getJWTToken(client, xrayClientId, xrayClientSecret);
        logger.info("JWT Token is {}", token);

        // Call the XRay Export URL..
        String url = XRAY_REST_EXPORT_URL + "?keys=" + xrayTests;
        logger.info("XRay Get Export URL :- " + url);
        client.property("accept", "application/zip");
        WebTarget target = client.target(url);
        Response response = target.request()
                .header("Authorization", "Bearer " + token)
                .get();

        if (response.getStatus() == HttpStatus.SC_OK) {
            // The XRay response is in zip file format so unzip it to the target folder..
            unzipDownloadedFeatureFile(response);
        } else {
            throw new RuntimeException("No feature files generated, response code " +  response.getStatus());
        }
    }

    /*
        Update the status of the specified JIRA issue.
     */
    private static synchronized void updateJiraIssueStatus(String jiraIssue, String authToken,
                                                           Client client) throws IOException {
        String jiraUpdateStatusUrl = XRayInstance.getJiraRootUrl() + "issue//" + jiraIssue + "//transitions";
        String body = "{\"transition\": {\"id\": \"31\"}}";

        logger.info("Launching update JIRA issue on " + jiraUpdateStatusUrl + " with body " + body);

        // Execute the update request..
        WebTarget target = client.target(jiraUpdateStatusUrl);
        Response response = target.request(MediaType.APPLICATION_JSON)
                .header("Authorization", authToken)
                .post(Entity.json(body));

        if (response.getStatus() == HttpStatus.SC_NO_CONTENT) {
            logger.info("We have successfully updated the XRay Status");
        } else {
            // The JIRA record update failed so throw an exception.
            throw new RuntimeException("XRay Issue update failed with response code " +  response.getStatus());
        }
    }

    /*
        If a filter query is received, we only want to use the first item retrieved from the filter.
     */
    private static synchronized String getFirstIssueFromFilter(String xrayFilterId, String authToken,
                                                               Client client) throws IOException {
        String jiraSearchUrl = XRayInstance.getJiraRootUrl() + "search" + "?jql=" + xrayFilterId + "&maxResults=1&fields=key";

        logger.info("Getting first issue from filter URL {}", jiraSearchUrl);

        // Execute the query against the filter..
        WebTarget target = client.target(jiraSearchUrl);
        Response response = target.request(MediaType.APPLICATION_JSON)
                .header("Authorization", authToken)
                .get();

        if (response.getStatus() == HttpStatus.SC_OK) {
            logger.info("We have successfully got the XRay Issue to process {}", response.getStatus());
            JsonNode parent = new ObjectMapper().readTree(response.readEntity(String.class));
            int total = parent.get("total").getIntValue();

            if (total == 0) {
                // The JIRA filter has returned zero rows, this isn't an issue so exit.
                logger.info("No executions in the queue to process at this time.");
                exit(0);
            }

            JsonNode issuesNode = parent.get("issues");
            Iterator<JsonNode> elements = issuesNode.iterator();
            JsonNode element = elements.next();
            return element.get("key").getTextValue();
        } else {
            // if the filter query fails for some reason we need to report this as a failure..
            throw new RuntimeException("The JIRA Filter query returned error code " + response.getStatus());
        }
    }

    /*
     * Unzip the file downloaded from the JIRA Export.
     */
    private static void unzipDownloadedFeatureFile(Response response) throws IOException {
        logger.info("Reading zip file content from REST response");
        InputStream inputStream = response.readEntity(InputStream.class);

        logger.info("Creating zip file from REST response");
        OutputStream outputStream = new FileOutputStream(XRAY_FEATURES_ZIP_PATH);
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        outputStream.close();

        logger.info("Create feature folder");
        File destDir = new File(XRAY_FEATURE_FOLDER);

        logger.info("Opening the downloaded zip file");
        ZipInputStream zis = new ZipInputStream(new FileInputStream(XRAY_FEATURES_ZIP_PATH));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File featureFile = createNewFeatureFile(destDir, zipEntry);
            logger.info("Extracting file {} from zip file", featureFile.getName());
            FileOutputStream fos = new FileOutputStream(featureFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }

    /*
     * Create a new file from the provided zip file entry.
     * @param destinationDir the directory in which to create the zip file
     * @param zipEntry file from the zip file used to create the file.
     * @return the new feature file
     * @throws IOException thrown if there is an issue processing the file.
     */
    private static File createNewFeatureFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    /*
    * Create the feature folder from where the tests will be run.
    */
    private static void makeTargetFeaturesDirectory() {
        if (!(new File(TEST_FOLDER_LOCATION)).mkdir()) {
            throw new RuntimeException("Failed to create test folder");
        }

        if (!(new File(XRAY_FEATURE_FOLDER)).mkdir()) {
            throw new RuntimeException("Failed to create feature folder");
        }
    }

    /*
     * If the tests to run have a Test Plan then we need to create an info file
     * for the test plan to be updated in XRay.
     *
     * @param jiraIssue the tests to be executed - passed in via environment variable
     * @param client JAXRS client used to make REST call to JIRA to get issue type.
     * @param basicAuth the authorisation token used to connect to JIRA.
     * @throws IOException
     */
    private static void createInfoFileIfRequired(String jiraIssue, Client client, String basicAuth) throws IOException {
        // Get the issue from JIRA
        String jiraIssueUrl = XRayInstance.getJiraRootUrl() + "//issue//" + jiraIssue;
        logger.info("JIRA Issue URL {}", jiraIssueUrl);
        WebTarget target = client.target(jiraIssueUrl);
        Response response = target.request(MediaType.APPLICATION_JSON)
                .header("Authorization", basicAuth)
                .get();

        // We only need to create the info type is the issue is a Test Plan
        if (response.getStatus() == HttpStatus.SC_OK) {
            logger.info("We have got the XRay Issue details {}", response.getStatus());
            JsonNode parent = new ObjectMapper().readTree(response.readEntity(String.class));
            String issueTypeName = parent.path("fields").path("issuetype").path("name").getTextValue();
            logger.info("Issue Type {}", issueTypeName);

            if (StringUtils.equals(issueTypeName, TEST_PLAN)) {
                logger.info("Need to create info file for Test Plan!");
                createInfoFile(jiraIssue);
            }
        } else {
            throw new RuntimeException("Unable to get JIRA issue type, error code " + response.getStatus());
        }
    }

    /*
     * Creates the info file and associates the test execution with the test plan.
     * @param the Test Plan Id
     * @throws IOException thrown if there is an issue building the info file.
     */
    private static void createInfoFile(String testPlanId) throws IOException {
        Fields fields = new Fields();
        Project project = new Project();
        project.setKey(testPlanId.substring(0, testPlanId.indexOf("-")));
        fields.setProject(project);
        fields.setSummary("Test Execution for Cucumber Execution");

        Issuetype issueType = new Issuetype();

        String jiraExecutionTypeId = XRayInstance.getXrayExecutionTypeId();

        issueType.setId(jiraExecutionTypeId);
        fields.setIssuetype(issueType);
        XrayIssue xrayIssue = new XrayIssue();
        xrayIssue.setFields(fields);

        XrayFields xrayFields = new XrayFields();
        xrayFields.setTestPlanKey(testPlanId);

        xrayIssue.setXrayFields(xrayFields);
        ObjectMapper mapper = new ObjectMapper();

        String jsonInString = mapper.writeValueAsString(xrayIssue);

        logger.info("Xray info file content {}", jsonInString);
        mapper.writeValue(new File(XRayInstance.INFO_URL), xrayIssue);
    }
}